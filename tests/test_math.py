from time import sleep


def test_math_still_works():
    sleep(2)
    assert 2 + 2 == 4


def test_incorrect_calculation():
    sleep(3)
    assert 2 > 5

